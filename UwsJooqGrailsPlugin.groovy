import ie.uws.jooq.JooqService

class UwsJooqGrailsPlugin {
    def version = "0.1.1"
    def grailsVersion = "2.0 > *"
    def pluginExcludes = [
        "grails-app/controllers/**",
        "grails-app/domain/**",
        "grails-app/taglib/**",
        "grails-app/views/**"
    ]
    def title = "uws-jOOQ Plugin"
    def author = "Marcin Świerczyński"
    def authorEmail = "marcin@uws.ie"
    def description = 'Integrates jOOQ library into Grails development process'
    def documentation = "https://bitbucket.org/uws/jooqgrailsplugin/src/920f61736557a60b439b40193d643169f150d23a/README.md?at=master"
    def license = "APACHE"
    def organization = [name: "UWS Software Service", url: "http://uws-software-service.com"]
    def developers = [
        [ name: "Marcin Szlagor", email: "marcin.szlagor@uws.ie" ],
        [ name: "Grzegorz Gajos", email: "grzegorz@uws.ie" ]
    ]
    def issueManagement = [url: "https://bitbucket.org/uws/jooqgrailsplugin/issues"]
    def scm = [ url: "https://bitbucket.org/uws/jooqgrailsplugin/src" ]

    def doWithSpring = {
        jooqService(JooqService) {
            grailsApplication = ref('grailsApplication')
        }
    }
}
