package ie.uws.jooq

import org.codehaus.groovy.grails.commons.GrailsApplication
import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.springframework.jdbc.datasource.DataSourceUtils

import java.sql.Connection

/**
 * @author Marcin Szlagor
 */
class JooqDSLContext {

    public static DSLContext getByDataSource(GrailsApplication application, String dataSourceName) {
        def dataSource = application.mainContext.getBean(dataSourceName)
        if (dataSource) {
            Connection connection = DataSourceUtils.getConnection(dataSource)
            return DSL.using(connection)
        }
    }
}
