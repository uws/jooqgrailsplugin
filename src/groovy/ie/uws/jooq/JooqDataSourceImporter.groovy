package ie.uws.jooq

import grails.util.Environment
import groovy.util.logging.Log4j
import groovy.xml.MarkupBuilder
import ie.uws.jooq.util.ConfigReader
import ie.uws.jooq.util.JooqDialectImporter
import org.codehaus.groovy.grails.commons.cfg.ConfigurationHelper

import java.sql.Connection
import java.sql.DriverManager

/**
 * @author Marcin Szlagor
 */
@Log4j
class JooqDataSourceImporter {

    private static final String JOOQ_GENERATOR_CLASS = 'org.jooq.util.DefaultGenerator'
    private static final String JOOQ_CONFIG_PREFIX = 'jooq'
    private static final String DEFAULT_CONFIG_DIRECTORY = 'grails-app/conf/jooq/'

    private String dataSource
    private String xmlConfigDir
    private Map config

    private static Connection dbConnection

    protected static Connection getConnection(Map dataSource) {
        dbConnection ?: DriverManager.getConnection(dataSource.url as String, dataSource.username, dataSource.password)
    }

    JooqDataSourceImporter(String dataSource) {
        this.dataSource = dataSource
        config = new ConfigReader().read(ConfigReader.DATA_SOURCE_CLASS).merge(mergeConfigFiles())
        xmlConfigDir = jooqConfig().xmlConfigDir ?: DEFAULT_CONFIG_DIRECTORY
    }

    private static ConfigObject mergeConfigFiles() {
        ConfigurationHelper.initConfig(new ConfigReader().read(ConfigReader.CONFIG_CLASS))
        return ConfigurationHelper.loadConfigFromClasspath(Environment.current.name)
    }

    public void generateConfigXml() {
        saveConfigFile(generateConfigFile())
    }

    private String generateConfigFile() {

        def writer = new StringWriter()
        def dataSource = dataSource()

        new MarkupBuilder(writer).configuration('xmlns': 'http://www.jooq.org/xsd/jooq-codegen-3.5.0.xsd') {
            jdbc {
                driver(dataSource.driverClassName)
                url(dataSource.url)
                user(dataSource.username)
                password(dataSource.password)
            }
            generator {
                name(JOOQ_GENERATOR_CLASS)
                database {
                    name(dialect())
                    writer << "\n"
                    mkp.comment("<includes>${includes()}</includes>")
                    writer << "\n"
                    mkp.comment("<inputSchema>${databaseName()}</inputSchema>")
                }
                target {
                    packageName(generatedClassPackageName())
                    directory(jooqConfig().generatedClassOutputDirectory)
                }
            }
        }
        writer
    }

    private Map dataSource() {
        config[dataSource]
    }

    private Map jooqConfig() {
        config[JOOQ_CONFIG_PREFIX]
    }

    private String generatedClassPackageName() {
        def jooqConfig = jooqConfig()
        if (jooqConfig.generatedClassPackageName && jooqConfig.generatedClassPackageName."$dataSource") {
            return jooqConfig.generatedClassPackageName."$dataSource"
        }
        return "jooq.${dataSource}"
    }

    private String dialect() {
        String dialect = new JooqDialectImporter(databaseProductName: connectionMetaData.databaseProductName).parse()
        if (!dialect) {
            println("Could not resolve 'database.name' entry. Please update $jooqConfigFileName")
        }
        return dialect
    }

    private String includes() {
        String databaseName = databaseNameFromConnection
        if (!databaseName) {
            println("Could not resolve 'includes' entry. Please update $jooqConfigFileName")
            return ""
        }

        "${databaseName}.*"
    }

    private String databaseName() {
        String databaseName = databaseNameFromConnection
        if (!databaseName) {
            println("Could not resolve 'inputSchema' entry. Please update $jooqConfigFileName")
        }
        return databaseName
    }

    private String getJooqConfigFileName() {
        "${dataSource}.xml"
    }

    private String getDatabaseNameFromConnection() {
        return connectionMetaData.hasProperty('database') ? connectionMetaData.database : ""
    }

    private void saveConfigFile(String configFileContent) {
        String xmlDirectory = new File(xmlConfigDir).absolutePath // get rid of trailing slash if any
        File folder = new File(xmlDirectory)
        File xmlFile = new File(folder, jooqConfigFileName)
        if (!folder.exists()) {
            folder.mkdirs()
        }
        xmlFile << configFileContent
    }

    private getConnectionMetaData() {
        getConnection(dataSource())?.metaData
    }
}
