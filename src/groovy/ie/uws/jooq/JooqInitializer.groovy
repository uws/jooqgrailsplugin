package ie.uws.jooq

import groovy.io.FileType
import ie.uws.jooq.util.ConfigReader
import org.jooq.util.GenerationTool

/**
 * @author Marcin Szlagor
 */
class JooqInitializer {

    void generateClasses() {
        ConfigObject config = new ConfigReader().read(ConfigReader.CONFIG_CLASS)
        // We tend to use actual config files rather then configured data sources
        new File(config.jooq.xmlConfigDir).eachFile(FileType.FILES) { File xmlConfigFile ->
            GenerationTool.main(xmlConfigFile.absolutePath)
            println "Generating tables classes using ${xmlConfigFile.absolutePath}"
        }
    }
}
