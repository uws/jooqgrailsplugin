package ie.uws.jooq

class JooqRunner {

    void init() {
        new JooqInitializer().generateClasses()
    }

    void generateConfig() {
        new JooqXmlConstructor().generateConfigXml()
    }

}
