package ie.uws.jooq.util

/**
 * @author Marcin Szlagor
 */
class JooqDialectImporter {

    private static List <String> jooqDialects = [
        'org.jooq.util.ase.ASEDatabase',
        'org.jooq.util.cubrid.CUBRIDDatabase',
        'org.jooq.util.db2.DB2Database',
        'org.jooq.util.derby.DerbyDatabase',
        'org.jooq.util.h2.H2Database',
        'org.jooq.util.hsqldb.HSQLDBDatabase',
        'org.jooq.util.ingres.IngresDatabase',
        'org.jooq.util.mysql.MySQLDatabase',
        'org.jooq.util.oracle.OracleDatabase',
        'org.jooq.util.postgres.PostgresDatabase',
        'org.jooq.util.sqlite.SQLiteDatabase',
        'org.jooq.util.sqlserver.SQLServerDatabase',
        'org.jooq.util.sybase.SybaseDatabase'
    ]

    String databaseProductName

    String parse() {
        jooqDialects.find { it.contains databaseProductName }
    }
}
