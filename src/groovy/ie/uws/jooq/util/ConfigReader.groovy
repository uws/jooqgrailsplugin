package ie.uws.jooq.util

import grails.util.Environment

/**
 * @author Marcin Szlagor
 */
class ConfigReader {

    public static final String CONFIG_CLASS = 'Config'
    public static final String DATA_SOURCE_CLASS = 'DataSource'

    ConfigObject read(String configClassName) {
        GroovyClassLoader classLoader = new GroovyClassLoader(getClass().classLoader)
        return new ConfigSlurper(Environment.current.name).parse(classLoader.loadClass(configClassName))
    }
}
